const { createApp } = Vue;

const app = createApp({
  data() {
    return {
      btc: {
        changePercent: -10,
        name: 'Bitcoin',
        symbol: 'BTC',
        price: 8400,
        img:
          'https://www.manejandodatos.es/wp-content/uploads/2018/02/vueJS.png',
        pricesWithDays: [
          { day: 'Lunes', value: 8400 },
          { day: 'Martes', value: 7900 },
          { day: 'Miercoles', value: 8200 },
          { day: 'Jueves', value: 9000 },
          { day: 'Viernes', value: 9400 },
          { day: 'Sabado', value: 10000 },
          { day: 'Domingo', value: 10200 }
        ]
      },
      color: 'f4f4f4'
    };
  },
  mounted() {
    console.log('Mounted...');
  },
  created() {
    console.log('Created...');
  },
  methods: {
    updateColor(color) {
      this.color =
        color ||
        this.color
          .split('')
          .reverse()
          .join('');
    }
  }
});

app.component('CoinDetail', {
  props: ['coin'],
  emits: ['change-color'],
  data() {
    return {
      showPrices: false,
      value: 0
    };
  },
  mounted() {
    console.log('Mounted iconDetail');
  },
  created() {
    console.log('Created iconDetail');
  },
  methods: {
    toggleShowPrices() {
      this.showPrices = !this.showPrices;

      this.$emit('change-color', this.showPrices ? 'FF96C8' : '3D3D3D');
    }
  },
  computed: {
    title() {
      return `${this.coin.name} - ${this.coin.symbol}`;
    },
    convertedValue() {
      if (!this.value) {
        return 0;
      }

      return this.value / this.coin.price;
    }
  },
  template: `
       <img
          width="200"
          height="200"
          :src="coin.img"
          :alt="coin.name"
          v-on:mouseover="toggleShowPrices"
          v-on:mouseout="toggleShowPrices"
        />

        <h1 :class="coin.changePercent > 0 ? 'green' : 'red'">
          {{ title }}
          <span v-if="coin.changePercent > 10">😎</span>
          <span v-else-if="coin.changePercent < 0">🙄</span>
          <span v-else>😅</span>

          <!-- <span v-show="changePercent > 0">😎</span> -->
          <!-- <span v-show="changePercent < 0">🙄</span> -->
          <!-- <span v-show="changePercent === 0">😅</span> -->
        </h1>
        <section>
          <input type="number" v-model="value" />
          <span>{{ convertedValue }}</span>
        </section>

        <slot name="text"></slot>
        <slot name="link"></slot>

        <section>
          <span v-on:click="toggleShowPrices">
            {{ showPrices ? 'Ocultar' : 'Ver Precios' }}
          </span>
        </section>

        <ul v-show="showPrices">
          <li
            class="uppercase"
            style="list-style: none; padding: 0; margin: 0"
            :class="{ orange: p.value > coin.price }"
            v-for="(p, i) in coin.pricesWithDays"
            :key="p.day"
          >
            <p>{{ i }} <b>{{ p.day }}</b>: <i>{{ p.value }}</i></p>
          </li>
        </ul>
        `
});

app.mount('#app');
